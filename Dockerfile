FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
   python3.6 \
   python3-pip \
   build-essential \
   git \
   dirmngr \
   gnupg \
   apt-transport-https \
   ca-certificates \
   software-properties-common
   
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 \
    && add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/' \
    && apt-get install -y \
    r-base 

RUN R --version
RUN R -e "install.packages(c('Rtsne','MASS','LaplacesDemon','ggplot2'),dependencies=TRUE, repos='http://cran.rstudio.com/')"


RUN pip3 install \
    numpy \
    pandas \
    keras \
    tensorflow

RUN git clone https://bitbucket.org/jspagnuolo/tessa.git tessa \
    && mkdir tessa_test 
## RUN TESSA Tests
RUN python3 ./tessa/BriseisEncoder/BriseisEncoder.py -tcr ./tessa/example_data/example_TCRmeta.csv -model ./tessa/BriseisEncoder/TrainedEncoder.h5 \
    -embedding_vectors ./tessa/BriseisEncoder/Atchley_factors.csv -output_TCR ./tessa_test/test.csv -output_VJ ./tessa_test/testVJ.csv -output_log ./tessa_test/test.log

RUN python3 ./tessa/Tessa_main.py -exp ./tessa/example_data/example_exp.csv -embedding ./tessa/example_data/example_TCRembedding.csv \
    -meta ./tessa/example_data/example_TCRmeta.csv -output_tessa ./tessa_test -within_sample_networks TRUE -predefined_b ./tessa/example_data/fixed_b.csv
